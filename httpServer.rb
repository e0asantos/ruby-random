require 'socket' # Provides TCPServer and TCPSocket classes
require 'net/http'
require 'net/https'
# Initialize a TCPServer object that will listen
# on localhost:2345 for incoming connections.
server = TCPServer.new('10.0.1.4', 2345)

# loop infinitely, processing one incoming
# connection at a time.
last=0
loop do

  # Wait until a client connects, then return a TCPSocket
  # that can be used in a similar fashion to other Ruby
  # I/O objects. (In fact, TCPSocket is a subclass of IO.)
  socket = server.accept

  # Read the first line of the request (the Request-Line)
  request = socket.gets



  # Log the request to the console for debugging
  STDERR.puts request

  response = "Hello World!\n"

  # We need to include the Content-Type and Content-Length headers
  # to let the client know the size and type of data
  # contained in the response. Note that HTTP is whitespace
  # sensitive, and expects each header line to end with CRLF (i.e. "\r\n")
  socket.print "HTTP/1.1 200 OK\r\n" +
               "Content-Type: text/plain\r\n" +
               "Content-Length: #{response.bytesize}\r\n" +
               "Connection: close\r\n"

  # uri = URI.parse("http://10.0.1.6")
  # http = Net::HTTP.new(uri.host, uri.port)
  # # http.use_ssl = true
  # # http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  # request = Net::HTTP::Post.new("/v1.1/auth")
  # request.add_field('Content-Type', 'application/json')
  # request.body = {'credentials' => {'username' => 'username', 'key' => 'key'}}
  # response = http.request(request)

  

  # Print a blank line to separate the header from the response body,
  # as required by the protocol.
  socket.print "\r\n"

  # Print the actual response body, which is just "Hello World!\n"
  socket.print response
  if last==0
    puts "ON"
    # dad9f471/350b47c3
    uri = URI('https://lineaccess.mx/anet_devices/sendToDevice')
    https = Net::HTTP.new(uri.host,uri.port)
    https.use_ssl = true
    req = Net::HTTP::Post.new(uri.path,initheader = {'Content-Type' =>'application/x-www-form-urlencoded'})
    req['device'] = 'dad9f471'
    req['content'] = 'ola k ase'
    req.body="device=dad9f471&content=on"

    res = https.request(req)
    # res = Net::HTTP.post_form(uri, 'device' => 'dad9f471', 'content' => 'ON')
    # puts res.body


    last=1
    # puts "ON"
  else
    puts "OFF"
    uri = URI('https://lineaccess.mx/anet_devices/sendToDevice')
    https = Net::HTTP.new(uri.host,uri.port)
    https.use_ssl = true
    req = Net::HTTP::Post.new(uri.path,initheader = {'Content-Type' =>'application/x-www-form-urlencoded'})
    req['device'] = 'dad9f471'
    req['content'] = "ola k ase"
    req.body="device=dad9f471&content=off"
    res = https.request(req)
    # puts "OFF"
    last=0
  end

  # Close the socket, terminating the connection
  socket.close
end