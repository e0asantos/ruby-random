'''
VOLUME CALCULATION STL binary MODELS
Author: Mar Canet (mar.canet@gmail.com) - september 2012
Description: useful to calculate cost in a 3D printing ABS or PLA usage
'''


class STLUtils

	@normals = []
	@points = []
	@triangles = []
	@bytecount = []
	@fb = [] # debug list
	@f=nil
	def resetVariables()
		@normals = []
		@points = []
		@triangles = []
		@bytecount = []
		@fb = [] # debug list
	end
		
	# Calculate volume fo the 3D mesh using Tetrahedron volume
	# based in: http://stackoverflow.com/questions/1406029/how-to-calculate-the-volume-of-a-3d-mesh-object-the-surface-of-which-is-made-up
	def signedVolumeOfTriangle(p1, p2, p3)
		# puts p1.inspect,p2.inspect,p3.inspect
		v321 = p3[0]*p2[1]*p1[2]
		v231 = p2[0]*p3[1]*p1[2]
		v312 = p3[0]*p1[1]*p2[2]
		v132 = p1[0]*p3[1]*p2[2]
		v213 = p2[0]*p1[1]*p3[2]
		v123 = p1[0]*p2[1]*p3[2]
		return (1.0/6.0)*(-v321 + v231 + v312 - v132 - v213 + v123)
	end

	def unpack( sig, l)
		s = @f.read(l)
		@fb.push(s)
		# puts "---"+sig
		# puts s.unpack('H*')
		# puts s.unpack(sig).inspect
		marray=s.unpack(sig)
		
		if marray[0]!=nil
			marray[0]=marray[0].to_f
		end
		if marray[1]!=nil
			marray[1]=marray[1].to_f
		end
		if marray[2]!=nil
			marray[2]=marray[2].to_f
		end
		return marray
	end

	def read_triangle()
		n  = unpack("<3f*", 12)
		p1 = unpack("<3f*", 12)
		p2 = unpack("<3f*", 12)
		p3 = unpack("<3f*", 12)
		b  = unpack("<h*", 2)
		
		@normals.push(n)
		l = @points.length
		@points.push(p1)
		@points.push(p2)
		@points.push(p3)
		@triangles.push(l, l+1, l+2)
		@bytecount.push(b[0])
		return signedVolumeOfTriangle(p1,p2,p3)
	end

	def read_length()
   		length = @f.read(4).unpack("@i")
   		return length[0]
   	end

	def read_header()
		@f.seek(@f.tell()+80)
	end
		
	def cm3_To_inch3Transform( v)
		return v*0.0610237441
	end
		
	def calculateWeight(volumeIn_cm)
		return volumeIn_cm*1.04

	end

	
	
	def calculateVolume(infilename, unit)
		puts infilename
		resetVariables()
		totalVolume = 0
		begin
			@f = open( infilename, "rb")
			read_header()
			l = read_length()
			puts "total triangles:",l
			begin
				while true
					totalVolume +=read_triangle()
				end
			rescue Exception => e
				#print e
				# puts e.backtrace.inspect  
				puts "End calculate triangles volume"
			end
			#print len(self.normals), len(self.points), len(self.triangles), l, 
			if unit=="cm"
				totalVolume = (totalVolume/1000)
				puts "Total volume:", totalVolume,"cm"
			else
				totalVolume = cm3_To_inch3Transform(totalVolume/1000)
				puts "Total volume:", totalVolume,"inch"
			end
		rescue Exception => e
			puts e

		end
		return totalVolume
	end
end



  
  utils=STLUtils.new
  utils.calculateVolume("galaxy.stl","cm")

# if __name__ == '__main__':
# 	if len(sys.argv)==1:
# 		print "Define model to calculate volume ej: python mesure_volume.py torus.stl"
# 	else:
# 		mySTLUtils = STLUtils()
# 		if(len(sys.argv)>2 and sys.argv[2]=="inch"):
# 			mySTLUtils.calculateVolume(sys.argv[1],"inch")
# 		else:
# 			mySTLUtils.calculateVolume(sys.argv[1],"cm")